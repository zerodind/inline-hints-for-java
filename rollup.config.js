//@ts-check
import typescript from "@rollup/plugin-typescript";
import { terser } from "rollup-plugin-terser";

const isProduction = process.env["PRODUCTION"] === "true";

/** @type import("rollup").RollupOptions */
const config = {
	input: "src/main/ts/extension.ts",
	output: {
		file: "dist/inline-hints-for-java.js",
		format: "cjs",
		sourcemap: !isProduction
	},
	external: [	"vscode" ],
	plugins: getPlugins()
}

function getPlugins() {
	const plugins = [
		typescript()
	];

	if (isProduction) {
		plugins.push(terser())
	}

	return plugins;
}

export default config;
