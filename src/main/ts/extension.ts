import { ExtensionContext } from "vscode";
import ParameterHintProvider from "providers/ParameterHintProvider";

export function activate(context: ExtensionContext) {
	context.subscriptions.push(new ParameterHintProvider());
}
