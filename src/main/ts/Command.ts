import { commands } from "vscode";

export enum Command {
	PROVIDE_PARAMETER_HINTS = "java.project.provideParameterHints"
}

export async function executeJavaCommand<T>(command: Command, ...params: any[]): Promise<T | undefined> {
	return await commands.executeCommand("java.execute.workspaceCommand", command, ...params);
}
