export default interface ParameterHint {
	text: string;
	line: number;
	column: number;
	isVarargs: boolean;
}
