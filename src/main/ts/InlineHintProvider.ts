import { DecorationOptions, Disposable, TextDocument, TextDocumentChangeEvent, TextEditor, TextEditorDecorationType, window, workspace } from "vscode";

export default abstract class InlineHintProvider implements Disposable {

	private readonly disposables: Disposable[];
	private readonly decorationCache: Map<string, DecorationOptions[]>;
	private readonly decorationType: TextEditorDecorationType;

	public constructor(decorationType: TextEditorDecorationType) {
		this.disposables = [
			workspace.onDidChangeTextDocument(this.documentChanged, this),
			workspace.onDidCloseTextDocument(this.documentClosed, this),
			window.onDidChangeVisibleTextEditors(this.visibleEditorsChanged, this)
		];
		this.decorationCache = new Map();
		this.decorationType = decorationType;
		this.visibleEditorsChanged(window.visibleTextEditors);
	}

	public dispose(): void {
		this.disposables.forEach(disposable => disposable.dispose());
		this.decorationType.dispose();
	}

	private async documentChanged(event: TextDocumentChangeEvent): Promise<void> {
		if (event.contentChanges.length !== 0) {
			const decorations = await this.getNewDecorations(event.document);
			for (const editor of window.visibleTextEditors) {
				if (editor.document.languageId === "java" && editor.document === event.document) {
					editor.setDecorations(this.decorationType, decorations);
				}
			}
		}
	}

	private documentClosed(document: TextDocument): void {
		if (document.languageId === "java") {
			this.decorationCache.delete(document.uri.toString());
		}
	}

	private async visibleEditorsChanged(visibleEditors: TextEditor[]): Promise<void> {
		for (const editor of visibleEditors) {
			if (editor.document.languageId === "java") {
				editor.setDecorations(
					this.decorationType,
					await this.getNewOrCachedDecorations(editor.document)
				);
			}
		}
	}

	private async getNewOrCachedDecorations(document: TextDocument): Promise<DecorationOptions[]> {
		return this.decorationCache.get(document.uri.toString())
			?? this.getNewDecorations(document);
	}

	private async getNewDecorations(document: TextDocument): Promise<DecorationOptions[]> {
		const versionBeforeRequest = document.version;
		const newDecorations = await this.provideHintDecorations(document);
		const versionAfterRequest = document.version;
		if (versionBeforeRequest !== versionAfterRequest) {
			await this.waitForDocumentChangesToEnd(document);
			return this.getNewDecorations(document);
		}
		this.decorationCache.set(document.uri.toString(), newDecorations);
		return newDecorations;
	}

	private async waitForDocumentChangesToEnd(document: TextDocument): Promise<void> {
		let version = document.version;
		return new Promise((resolve) => {
			const iv = setInterval(() => {
				if (document.version === version) {
					clearInterval(iv);
					resolve();
				}
				version = document.version;
			}, 400);
		});
	}

	protected abstract provideHintDecorations(document: TextDocument): Promise<DecorationOptions[]>;

}
