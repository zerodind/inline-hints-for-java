import { DecorationOptions, Range, TextDocument, ThemeColor, window } from "vscode";
import InlineHintProvider from "InlineHintProvider";
import ParameterHint from "models/ParameterHint";
import { Command, executeJavaCommand } from "Command";

export default class ParameterHintProvider extends InlineHintProvider {

	public constructor() {
		super(window.createTextEditorDecorationType({
			before: {
				color: new ThemeColor("inlineHintsForJava.parameterHint.foreground")
			},
			after: {
				color: new ThemeColor("inlineHintsForJava.parameterHint.foreground")
			}
		}));
	}

	protected async provideHintDecorations(document: TextDocument): Promise<DecorationOptions[]> {
		const parameterHints = await this.requestParameterHints(document.uri.toString());
		return parameterHints.map(hint => <DecorationOptions>{
			range: new Range(hint.line, hint.column, hint.line, hint.column),
			renderOptions: {
				after: {
					contentText: hint.isVarargs
						? "..." + hint.text + ": "
						: hint.text + ": "
				}
			}
		});
	}

	private async requestParameterHints(uri: string): Promise<ParameterHint[]> {
		return await executeJavaCommand(Command.PROVIDE_PARAMETER_HINTS, uri) || [];
	}

}
