package com.gitlab.zerodind.ih4j;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class PluginActivator implements BundleActivator {

	@Override
	public void start(BundleContext context) {}

	@Override
	public void stop(BundleContext context) {}

}
