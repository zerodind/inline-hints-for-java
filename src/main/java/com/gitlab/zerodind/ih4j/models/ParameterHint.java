package com.gitlab.zerodind.ih4j.models;

public class ParameterHint {

	public final String text;
	public final int line;
	public final int column;
	public final boolean isVarargs;

	public ParameterHint(String text, int line, int column, boolean isVarargs) {
		this.text = text;
		this.line = line;
		this.column = column;
		this.isVarargs = isVarargs;
	}

}
