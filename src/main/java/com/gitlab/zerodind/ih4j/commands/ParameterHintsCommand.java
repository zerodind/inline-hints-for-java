package com.gitlab.zerodind.ih4j.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gitlab.zerodind.ih4j.models.ParameterHint;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.manipulation.CoreASTProvider;
import org.eclipse.jdt.ls.core.internal.JDTUtils;
import org.eclipse.jdt.ls.core.internal.JobHelpers;
import org.eclipse.jdt.ls.core.internal.handlers.DocumentLifeCycleHandler;

@SuppressWarnings("restriction")
public class ParameterHintsCommand {

	public static List<ParameterHint> provide(String uri) {
		JobHelpers.waitForJobs(DocumentLifeCycleHandler.DOCUMENT_LIFE_CYCLE_JOBS, null);

		ITypeRoot typeRoot = JDTUtils.resolveTypeRoot(uri);
		if (typeRoot == null) {
			return Collections.emptyList();
		}

		CompilationUnit cu = CoreASTProvider.getInstance().getAST(
			typeRoot,
			CoreASTProvider.WAIT_YES,
			new NullProgressMonitor()
		);
		if (cu == null) {
			return Collections.emptyList();
		}

		return new ParameterVisitor(cu).collectHints();
	}

	private static class ParameterVisitor extends ASTVisitor {
		private final ArrayList<ParameterHint> hints;
		private final CompilationUnit cu;

		private ParameterVisitor(CompilationUnit cu) {
			hints = new ArrayList<>();
			this.cu = cu;
		}

		public ArrayList<ParameterHint> collectHints() {
			cu.accept(this);
			return hints;
		}

		@Override
		public boolean visit(MethodInvocation node) {
			visitMethodArguments(node.resolveMethodBinding(), node.arguments());
			return super.visit(node);
		}

		@Override
		public boolean visit(SuperMethodInvocation node) {
			visitMethodArguments(node.resolveMethodBinding(), node.arguments());
			return super.visit(node);
		}

		@Override
		public boolean visit(ClassInstanceCreation node) {
			visitMethodArguments(node.resolveConstructorBinding(), node.arguments());
			return super.visit(node);
		}

		@Override
		public boolean visit(SuperConstructorInvocation node) {
			visitMethodArguments(node.resolveConstructorBinding(), node.arguments());
			return super.visit(node);
		}

		@Override
		public boolean visit(EnumConstantDeclaration node) {
			visitMethodArguments(node.resolveConstructorBinding(), node.arguments());
			return super.visit(node);
		}

		private void visitMethodArguments(IMethodBinding methodBinding, List<?> arguments) {
			if (methodBinding == null) {
				return;
			}
			IMethod method = (IMethod) methodBinding.getJavaElement();
			if (method == null) {
				return;
			}

			try {
				String[] parameterNames = method.getParameterNames();
				for (int i = 0; i < arguments.size(); i++) {
					ASTNode argument = (ASTNode) arguments.get(i);
					hints.add(new ParameterHint(
						parameterNames[i],
						cu.getLineNumber(argument.getStartPosition()) - 1,
						cu.getColumnNumber(argument.getStartPosition()),
						methodBinding.isVarargs() && i == parameterNames.length - 1
					));
				}
			} catch (JavaModelException | ArrayIndexOutOfBoundsException e) {
				// Ignore
			}
		}
	}

}
