package com.gitlab.zerodind.ih4j;

import java.util.List;

import com.gitlab.zerodind.ih4j.commands.ParameterHintsCommand;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.ls.core.internal.IDelegateCommandHandler;

@SuppressWarnings("restriction")
public class DelegateCommandHandler implements IDelegateCommandHandler {

	@Override
	public Object executeCommand(String commandId, List<Object> arguments, IProgressMonitor monitor) throws Exception {
		switch (commandId) {
			case Command.PROVIDE_PARAMETER_HINTS: {
				return ParameterHintsCommand.provide((String) arguments.get(0));
			}
			default: {
				throw new UnsupportedOperationException("Unsupported command!");
			}
		}
	}

}
