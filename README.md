# Inline Hints for Java
Provides inline hints for Java code.

## Currently supported hints
- [x] Parameter name hints
- [ ] Type hints
- [ ] Method chain hints
- [ ] Statement end hints

## Installation
Download the VSIX file [from a release][1], then install using [these instructions][2].
The extension will be released to the marketplace once I feel it's ready and I have time to spare.

[1]: https://gitlab.com/zerodind/inline-hints-for-java/-/releases
[2]: https://code.visualstudio.com/docs/editor/extension-marketplace#_install-from-a-vsix
